require 'open3'

class Ps3
  BUTTONS = %w(up right down left triangle circle cross square)
  PS3_BLUETOOTH_ADDRESS = '00:21:4f:db:6b:5c'
  BOOT_TIME = 9 # seconds

  class << self
    def play_show(options)
      new.play_show(options)
    end
  end

  def initialize
    unless emulator_running?
      start_emulator_agent
      boot_ps3
    end
  end

  def boot_ps3
    # unless ps3_running?
      log 'wait for PS3 to boot'
      sleep(23)
    # end
  end

  def start_emulator_agent
    unless emulator_running?
      log "starting emulator"
      exec_shell("nohup emu 00:21:4f:db:6b:5c 0 &")
      sleep(3) # needs some seconds to start the agent
    end
  end

  def emulator_running?
    exec_sync_shell("hcitool con | grep '00:21:4F:DB:6B:5C' | wc -l").strip == '1'
  end

  def ps3_running?
    exec_sync_shell("sudo hcitool cc 00:21:4f:db:6b:5c 2>&1") !~ /Can't create connection: Input/
  end

  def turn_ps3_off
    8.times { left }
    6.times { up }
    cross
    sleep(0.5)
    cross
  end

  def play_show(options = {})
    login_ede
    sleep(5) # wait PS3 Menu to load

    start_lovefilm_app
    sleep(45)

    lovefilm = Lovefilm.new(self)
    lovefilm.play_show name: "Doctor Who", season: 6, episode: 10
  end

  def login_ede
    8.times { left }
    # go full up and start from top
    5.times { down }
    4.times { up }
    cross
    sleep(3)
  end

  def start_lovefilm_app
    9.times { right }
    4.times { left }
    sleep(3)
    7.times { down }
    3.times { up }
    cross
  end

  BUTTONS.each do |button|
    define_method button do
      press_and_release(button)
    end
  end

  def press_and_release(button)
    press(button)
    sleep(0.06)
    release(button)
    sleep(0.06)
  end

  def press(button)
    exec_sync_shell(%Q|emuclient --event "#{button}(255)"|)
  end

  def release(button)
    exec_sync_shell(%Q|emuclient --event "#{button}(0)"|)
  end

  def exec_sync_shell(cmd)
    `#{cmd}`
  end

  def exec_shell(cmd)
    log "Executing #{cmd}"
    Open3.popen3(cmd)
  end

  def log(msg)
    puts msg
    $logger.info msg
  end
end
