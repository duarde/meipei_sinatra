# TODOs:

# Keyboard sometimes types keys wrong, maybe macros can help:
# http://gimx.fr/wiki/index.php?title=Macrosœ

class Lovefilm
  class Keyboard
    CHAR_ROW_1 = %w(q w e r t z u i     o p backspace)
    CHAR_ROW_2 = %w(a s d f g h j k     l numbers)
    CHAR_ROW_3 = %w(y x c v b n m space umlauts)

    attr_reader :remote
    def initialize(remote)
      @remote = remote
      @numbers_active = false
    end

    def enter_phrase(phrase)
      phrase.downcase.each_char do |char|
        if char =~ /\A\d+\Z/
          press_number(char.to_i)
        elsif char == ' '
          press_char('space')
        else
          press_char(char)
        end
      end
    end

    def press_char(char)
      log "Pressing char: '#{char}'"
      back_to_root
      index = case
        when idx = CHAR_ROW_1.index(char)
          2.times { remote.up }
          idx
        when idx = CHAR_ROW_2.index(char)
          remote.up
          idx
        when idx = CHAR_ROW_3.index(char)
          idx
        else
          log("Unkown char '#{char}'")
          0
      end
      index.times { remote.right }
      remote.cross
      sleep(0.05)
    end

    def press_number(number)
      log "Pressing number: '#{number}'"
      press_char('numbers')
      back_to_root
      @numbers_active = true
      2.times { remote.up }
      if number == 0
        9.times { remote.right }
      else
        (number - 1).times { remote.right }
      end
      remote.cross
    end

    def numbers_active?
      @numbers_active
    end

    def move_to_bottom_left
      2.times { remote.down }
      CHAR_ROW_1.size.times { remote.left }
    end

    def back_to_root
      move_to_bottom_left
      if numbers_active?
        9.times { remote.right }
        remote.up
        remote.cross
        move_to_bottom_left
      end
    end

    def log(msg)
      $logger.info "Lovefilm: #{msg}"
      puts "Log: #{msg}"
    end
  end

  attr_reader :remote
  def initialize(remote)
    @remote = remote
  end

  def skip_init_screen
    # lovafilm wants us to move our account to amazon
    remote.cross
    sleep(1)
  end

  def start(title)
    search(term)
    # play_show()
  end

  def play_show(name: "Doctor Who", season: 6, episode: 10)
    skip_init_screen
    search("#{name} #{season}")
    sleep(10)
    play_episode(episode)
  end

  def search(term)
    remote.triangle
    sleep(3) # seaarch form needs some seconds
    keyboard = Keyboard.new(remote)
    keyboard.enter_phrase(term)
    sleep(2)
    remote.up
    sleep(0.3)
    remote.cross
  end

  def play_episode(number = 1)
    16.times { remote.up }

    number.times { remote.down }

    remote.cross
    sleep(20)
    remote.cross
  end

  def log msg
    logger.info "Ps3: #{msg}"
  end

end
