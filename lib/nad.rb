class Nad < IrEmitter
  ACTIONS = {
    'on'          => "KEY_POWER",
    'off'         => "KEY_POWER2",
    '1'           => "KEY_NUMERIC_1",
    '2'           => "KEY_NUMERIC_2",
    'playstation' => "KEY_NUMERIC_3",
    'airplay'     => "KEY_NUMERIC_4",
    '5'           => "KEY_NUMERIC_5",
    '6'           => "KEY_NUMERIC_6",
    '7'           => "KEY_NUMERIC_7",
    '8'           => "KEY_NUMERIC_8",
    '9'           => "KEY_NUMERIC_9",
    'volume_up'   => "KEY_VOLUMEUP",
    'volume_down' => "KEY_VOLUMEDOWN",
    'mute'        => "KEY_MUTE",
    'sleep'       => "KEY_SLEEP",
  }
end
