require_relative "ir_emitter"
require_relative "led" unless RUBY_PLATFORM.downcase.include?("darwin")
require_relative "nad"
require_relative "screen"
require_relative "projector"
require_relative "ps3"
