class Screen < IrEmitter
  ACTIONS = {
    'up'   => "KEY_UP",
    'down' => "KEY_DOWN",
    'stop' => "KEY_STOP",
  }
end
