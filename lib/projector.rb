class Projector < IrEmitter
  ACTIONS = {
    'on'         => "KEY_POWER",
    'off'        => "KEY_POWER2",
    'bluescreen' => "KEY_BLUE",
  }
end
