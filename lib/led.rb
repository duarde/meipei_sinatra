require 'pi_piper'

class Led
  include PiPiper
  ACTIONS = {
    'on'  => 'on',
    'off' => 'off',
  }

  UnknownActionForDevice = Class.new(StandardError)

  class << self
    def exec(command, options = {})
      raise UnknownActionForDevice unless self::ACTIONS[command]
      send_signal(self::ACTIONS[command])
    end

    def send_signal(command)
      new.pin.send(command)
    end
  end

  def pin
    @pin ||= PiPiper::Pin.new(:pin => 18, :direction => :out)
  end
end
