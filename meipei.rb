require 'sinatra'
# require 'logger'
require_relative 'lib/items'

class Meipei < Sinatra::Application
  configure do
    set :logging, true
    set :root, File.dirname(__FILE__)
  end

  configure :development, :production do
    # console log to file
    log_path = "#{root}/log"
    Dir.mkdir(log_path) unless File.exist?(log_path)
    log_file = File.new("#{log_path}/#{settings.environment}.log", "a+")
    log_file.sync = true
    $stdout.reopen(log_file)
    $stderr.reopen(log_file)
  end

  get '/' do
    logger.info "loading data"
    erb :index
  end

  get '/pi/:item/:command' do
    $logger = logger
    msg = "Item '#{item}' runs command '#{command}'"
    logger.info msg
    item.exec(command, params: params)
    msg
  end

  get '/start' do
    $logger = logger
    Screen.exec('down')
    Screen.exec('down')
    Projector.exec('on')
    Projector.exec('on')
    p = Ps3.new
    p.play_show
  end

  def command
    params[:command]
  end

  def item
    item_name = params[:item].to_s
    item_name[0] = item_name[0].upcase
    begin
      Object.const_get(item_name)
    # rescue NameError
      # raise StandardError.new("Unknown Item #{item_name}")
    end
  end
end
